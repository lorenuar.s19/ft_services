#!/bin/bash

IMAGE_NAME="ft_server"

GRY="\033[30;1m"
RED="\033[31;1m"
GRN="\033[32;1m"
YEL="\033[33;1m"
RST="\033[m"

quit()
{
	printf "${RED}ERROR %d${RST}\n" $1
	exit $1
}

setup_minikube()
{
	printf "\n🔧 ${GRN}Starting minikube node${RST}\n"
	# Remove old kubernetes instances
	# minikube delete
	# Start new kubernetes instance
	minikube start --cpus=4 --driver=docker --addons metrics-server --addons metallb || quit $?

	printf "\n🔧 ${GRN}Applying MetalLB ConfigMap${RST}\n"
	kubectl apply -f srcs/metallb-config.yaml || quit $?
	kubectl describe configmap config -n metallb-system || quit $?
	# Sync minikube with local docker instance
	eval $(minikube docker-env) || quit $?

}

build_image()
{
	printf "\n🧪 ${GRN}Building Image %s${RST}\n" $1

	docker build -t $1 srcs/$1 || quit $?
	return $?
}

deploy_image()
{
	printf "\n🔧 ${GRN}Deploying Image %s${RST}\n" $1

	kubectl apply -f srcs/$1/$1-deploy.yaml || quit $?
	return $?
}

build_and_deploy()
{
	printf "\n🔧 ${GRN}Building and deploying images${RST}\n"

	LIST=( nginx )
	for target in "${LIST[@]}"
	do
		build_image $target && deploy_image $target || quit $?
	done

}

auto()
{
	setup_minikube

	build_and_deploy

	printf "\n🚀 ${GRN}Launching minikube dashboard${RST}\n"
	minikube dashboard


}

stop()
{
	minikube stop
}

delete_all()
{
	minikube delete -all
}

svc ()
{
	printf "\n🚀 ${GRN}Launching nginx service page${RST}\n"
	minikube service nginx-svc --https=true
}

usage()
{
	echo "\
$0 [option]
options :
    auto | a        - Build, Setup, Start & launch dashboard
    start | s       - Start Kubernetes cluster
    build | b		- Build and deploy docker images
    stop | k        - Stop Kubernetes cluster
    del-all         - Delete all kubernetes instances
"
}

case "$1" in
	"auto" | "a" ) auto;
	;;
	"start" | "s" ) setup_minikube;
	;;
	"build" | "b" ) build_and_deploy;
	;;
	"stop" | "k" ) stop;
	;;
	"del-all" ) delete_all;
	;;
	"svc" ) svc;
	;;
	* | "h" | "help" | "H" ) usage;
	;;
esac
