#!/bin/bash

IMAGE_NAME="nginx-alpine"

GRY="\033[30;1m"
RED="\033[31;1m"
GRN="\033[32;1m"
YEL="\033[33;1m"
RST="\033[m"

get_container_ip()
{
    printf '\033[44;37m\n=== Container IP :\n %s\n===\033[0m\n' $(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${IMAGE_NAME} )
}

build ()
{
    #rm_all
    printf "${GRN}Building Docker Image : ${IMAGE_NAME}${RST}\n"
    docker build --progress=tty -t $IMAGE_NAME .
}

rebuild ()
{
    rm_all
    printf "${GRN}Building Docker Image : ${IMAGE_NAME}${RST}\n"
    docker build --no-cache --progress=tty -t $IMAGE_NAME .
}

start ()
{
    printf "${GRN}Starting docker container in detached mode: ${IMAGE_NAME}${RST}\nID :\n"
    docker run --rm -dit  -p 80:80 -p 443:443 -h $IMAGE_NAME --name $IMAGE_NAME $IMAGE_NAME
    get_container_ip
}

interact ()
{
    local res=$(docker ps -f status=running --format "{{.ID}} : {{.Image}} > {{.Status}}" )
    if [[ -z $res ]]; then
        printf "${RED}ERROR : no container running${RST}\n"
    else
        get_container_ip
        printf "${GRN}Starting sh prompt in ${IMAGE_NAME} container${RST}\n"
        docker exec -it ${IMAGE_NAME} sh
    fi
}

stop ()
{
    local res=$(docker ps -aq)
    if [[ ! -z $res ]]; then
        printf "${YEL}Stopping all running docker containers${RST}\n"
        docker stop $res
    else
        printf "${RED}No docker containers are running ${RST}\n"
    fi
}

list()
{
    printf "${YEL}Listing docker containers${RST}\n"
    docker ps -a
}

rm_all()
{
    stop
    printf "${YEL}Removing all docker containers${RST}\n"
    local res=$(docker ps -aq)
    if [[ ! -z $res ]]; then
        docker rm $res
    else
        printf "${RED}No existing docker containers ${RST}\n"
    fi
}

usage ()
{
    echo "\
$0 [option] [param]
options :
    a | auto [i]                        - Build & Start ${IMAGE_NAME} detached or interactive
    b | build                           - Build the docker container.
    re | rebuild                        - Rebuild without using cache
    start                               - Start the ${IMAGE_NAME} docker container in detached mode.
    i | interact | int                  - Docker exec a bash prompt
    stop                                - Stop ALL the docker container.
    list | ls                           - List ALL docker containers.
    R | rm-all                          - Remove ALL docker containers
    help | h | H                        - Displays this help message.
    "
}

complete -W "build start stop list ls rm-all help h H" $0

case "$1" in
    "auto" | "a" )
        if [[ $# -eq 2 ]] && ( [[ "$2" == "int" ]] || [[ "$2" == "i" ]] ); then
            build; start; list; interact;
        else
            build; start; list;
        fi
    ;;
    "int" | "interact" | "i" )   interact;
    ;;
    "build" | "b")   build;
    ;;
    "rebuild" | "re" )  rebuild;
    ;;
    "start" )   start;
    ;;
    "stop" )   stop;
    ;;
    "list" | "ls" )   list;
    ;;
    "R" | "rm-all"    )   rm_all;
    ;;
    "help" | "h" | "H" | * )   usage;
    ;;
esac
