
# install required packages
apk update && apk add nginx openssl

# Create directories
mkdir /run/nginx/ www/

# move config files
mv nginx.conf /etc/nginx/.
mv index.html /www/.

# generate new SSL certificate and Key
openssl req -x509 -newkey rsa:4096 -nodes -days 5 \
	-subj '/CN=ft_services' \
	-keyout /etc/ssl/private/ft_services.key \
	-out /etc/ssl/certs/ft_services.crt

# Start NGINX
/usr/sbin/nginx -g "daemon off;"

echo Everything done.

sleep infinity & wait
